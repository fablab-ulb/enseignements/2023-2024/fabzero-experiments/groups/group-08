# Blog of group 8
This blog is the website of groupe 8 formed by :

- [Eva Dubar](https://eva-dubar-fablab-ulb-enseignements-2023-2024-fab-47725489fd9b1e.gitlab.io/)
- [Eliott Fontaine](https://eliott-fontaine-fablab-ulb-enseignements-2023-20-855578e83b3304.gitlab.io/)distribution services and privatization.
- [Gala Toscano-Aguilar](https://gala-toscano-fablab-ulb-enseignements-2023-2024--fb3931ac0d1259.gitlab.io/)
- [Milan Bontridder](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/milan.bontridder)

For the course ‘[2023-2024 ULB class “How To Make (almost) Any Experiments Using Digital Fabrication”](https://class-website-fablab-ulb-enseignements-2023-2024-c4a40531a3337f.gitlab.io/)’.