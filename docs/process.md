# Processus

## Notre groupe

Notre groupe nouvellement formé ne pouvait pas entreprendre un projet sans fonctionnement bien structuré. Voici la formation de notre groupe et son fonctionnement.

### Formation du groupe

Durant la semaine de formation des groupes, nous avions tous un objet représentant un sujet qui nous touche personnellement. Notre groupe de 4 personnes s'est réuni autour des objets suivants:

- [Eva Dubar](https://eva-dubar-fablab-ulb-enseignements-2023-2024-fab-47725489fd9b1e.gitlab.io/)
    - Objet : Un câble Ethernet représentant la Cybersécurité.
  
- [Eliott Fontaine](https://eliott-fontaine-fablab-ulb-enseignements-2023-20-855578e83b3304.gitlab.io/)
    - Objet: Un entonnoir représentant les problèmes de distribution et de privatisation d'eau potable.
  
- [Gala Toscano-Aguilar](https://gala-toscano-fablab-ulb-enseignements-2023-2024--fb3931ac0d1259.gitlab.io/)
    - Objet: Une couverture représentant les problèmes de personnes sans-chez-soi.
  
- [Milan Bontridder](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/milan.bontridder)
    - Objet: Une pâte et un briquet représentant le problème de conscientisation de la quantité de consommation d'énergie.


### Fonctionnement du groupe

[commentaire: Ce que l'on doit faire]: <> (
    Explain how your group works.
        What do you already have in place?
        How will you, as a group, structure your meeting?
        What roles have you identified for meetings and projects in general?
        How will you make decisions within your group?
        How will you communicate within your group?
)

Notre groupe s'entendait tout de suite très bien et on sentait donc qu'il n'y avait pas besoin d'être très strict dans no rapport. Nous avons néanmoins établi des règles pour nos réunions de groupe : 

- Essayer de gérer au mieux notre temps
- Ne pas se couper la parole
- Ecouter tout le monde et laisser tout le monde parler

Nous avons choisis de faire des rôles tournant lors des réunions, de commencer chacunes de nos réunions par une prise de météo générale. Nous nous sommes aussi réparties les tâches de manière assez naturelle en fonction des connaissances de chacuns.

Notre canal de communication principal était Discord, qui nous a permis de s'envoyer différentes ressources et communiquer assez facilement.


[Commentaire de Milan]: <> (Partie ci dessous à placer au bon endroit et vérifier qu'il n'y a pas de répétition.)

[Commentaire de Eva]: <> (Tu parles de la problématique?? c'est le bon endroit non?)



## Définition de la problématique 

Tout processus de sélection d’une problématique implique une démarche rigoureuse, depuis le stade du brainstorming jusqu’à l’identification des enjeux majeurs. Dans notre cas, le choix s’est porté sur une problématique cruciale à Bruxelles : le manque de logement et l’inégalité d’accès a un domicile. Cette décision a émergé d’un processus itératif, impliquant la contribution de chaque membre du groupe à partir d'objets concrets puis évoluant vers la définition d’arbres à problèmes. 

Le constat est clair : de nombreuses personnes à Bruxelles se trouvent exclues de l’accès à un logement décent, créant ainsi une crise majeure. Cependant, la question fondamentale qui sous-tend notre problématique est : « Pourquoi tout le monde n’a pas accès à un logement ? » 

En Belgique, près d’une personne sur cinq est en situation de pauvreté ou exclusion sociale, selon les chiffres officiels de Statbel en 2021. Ces données révèlent que 12,7% de la population est considérée comme « à risque de pauvreté monétaire », englobant des critères allant de la faible intensité de travail à la privation matérielle et sociale sévère, touchant au total 18,8% de la population. Cette situation est exarcerbée par une diminution de l’offre de logements sociaux de 2004 à 2021, passant de 3056 attributions annuelles à 2164. 

La demande de logements sociaux a explosé, avec 51615 demandes en 2021, et actuellement environ 55000 ménages (environ 120000 personnes) en attente d’un logement social, avec un temps d’attente estimé à environ 10 ans. Le boom démographique des 20 dernières années à Bruxelles a accentué la crise du logement, avec entre 17000 et 26000 logements actuellement recensés comme vides dans la capitale, totalisant environ 6,5 millions de mètres carrés non occupés (plus que la surface qu’Ixelles). 

En outre, les chiffres de 2022 recensent 7134 sans-abris à Bruxelles, soulignant l’urgence de la situation. Ces statistiques révèlent une réalité complexe et alarmante, justifiant pleinement notre engagement à explorer des solutions innovantes pour répondre au défi pressant du manque de logements à Bruxelles. 

## Hackathon
Le hackaton en carton a pour but le développement des premiers prototypes.



[Commentaire Milan]: <> (Il faudrait compléter cette partie ou la supprimer, Eva: jpense ca se supprime, jmet en comm au cas ou :  Contraintes, problèmes et ressourcesPour la semaine prototype en cartons, nous avons organisé un stand avec des prototypes représentatifs et non fonctionnels de notre problèmatique.
Les prototypes étaient disposé sur la table avec les fiches suivantes.)



#### Contraintes

- Réponse partielle ou complète à la problématique (la problématique étant les problèmes de manque d'habitations).
- Accessibilité (budget)
- Localisé à Bruxelles
- 


#### Problèmes

Voici notre arbre à problème: 
![](images/soltree.png)

Et notre arbre à solutions : 
![](images/soltree2.png)

#### Ressources

Nous avons utilisés différentes sources pour notre arbre à problème, les voici:

- [Belspo](https://www.belspo.be/belspo/brain-be/docum/media/54sci_fr.pdf)
- [La Strada](https://www.lastrada.brussels/portail/images/PDF/20161107-20170306_RAP_Denombrements_FR_BU20170616.pdf)
- [FéBul](https://www.febul.be/wp-content/uploads/2020/11/manifeste-web-pub-2019.pdf)
- <a href="https://syndicatsmagazine.be/belgique-pauvrete/#:~:text=Le%20risque%20de%20pauvret%C3%A9%20en,27%2C8%20%25)%20semble%20plus%E2%80%A6">Syndicats Magazine</a>
- [L'avenir](https://www.lavenir.net/regions/2021/12/15/il-y-aurait-entre-17000-et-26000-logements-vides-a-bruxelles-RJRVIZ7CZNFWNLHMQVA5NXRABY/)
- [rtbf](https://www.rtbf.be/article/contre-la-gentrification-ou-lorsque-bruxelles-se-transforme-au-detriment-des-quartiers-populaires-10775915)
- [printemps des sciences](https://sciences.brussels/printemps/projet-expo/la-gentrification-le-long-du-canal-de-bruxelles/)
- [l'ilot](https://ilot.be/sortie-de-prison-et-sans-abrisme/)
- [rtbf](https://www.rtbf.be/article/les-logements-vides-sont-insuffisamment-sanctionnes-en-region-bruxelloise-10036725)
- [cbcs](https://cbcs.be/mythes-realite-et-discours-du-logement-vide-a-bruxelles/)
- [alterechos](https://www.alterechos.be/loi-anti-squat-quand-le-federal-criminalise-le-droit-au-logement/#:~:text=Que%20dit%20la%20loi%20anti,du%20Roi%20ordonne%20son%20expulsion.)
- [droits quotidiens](https://www.droitsquotidiens.be/fr/question/quest-ce-que-je-risque-si-joccupe-une-maison-sans-autorisation-squat-en-wallonie)
- [Bruxelles Dévie](https://www.instagram.com/p/CzRbaq7t2sm/?img_index=1)
- [Sans-abrisme et errance : entre causes et conséquences, Christelle Achard](https://www.cairn.info/revue-le-sociographe-2016-1-page-85.html)
- [Le Vif](https://www.levif.be/belgique/une-carte-interactive-pour-lutter-contre-les-logements-vides-a-bruxelles/)
- [RDBH](https://squatbelgium.noblogs.org/files/2017/08/audition-RBDH.pdf)
- [Sans-abrisme et urgence sociale à Bruxelles : l'échec d'une expérience, Bernard Francq](https://www.cairn.info/revue-espaces-et-societes-2004-1-page-159.htm)
- [RBDH, "habiter" exposition sonore](http://rbdh-bbrow.be/)



### Les prototypes

Le problème pressant du manque de logement à Bruxelles a été au cœur de notre réflexion lors de l’Hackathon, un événement défini comme une « journée d’innovation où les participant se réunissent pour générer des idées et concevoir des solutions sur une très courte période ». 

Au cours de ces séances dédiées à l’élaboration de nos prototypes, nous avons cherché des solutions novatrices pour aborder cette crise du logement. En quelques heures seulement, nous devions non seulement choisir notre solution, mais également créer un premier prototype pour le présenter lors de la place des marchés. Cette contrainte temporelle a stimulé notre créativité et nous a poussés à explorer diverses idées pour résoudre la problématique complexe du manque de logement à Bruxelles. 

![Photo générale de tous les prototypes ](images/all_proto.jpg)
 

 Nous avons classé les solutions dans différente catégorie que nous avons nommé "axes".

#### Recensement des logements inoccupés  

##### Calle à porte

Le premier portotype consiste en une « calle » à glisser entre la porte et le cadre, une méthode déjà utilisée par les squatteurs. Le fonctionnement est simple : une fois insérée, la « calle » reste en place pendant quelques jours/semaines. Si le logement est occupé, la « calle » tombera lorsque les habitants ouvriront leur porte d’entrée, si le logement est inoccupé on retrouvera cette « calle » intacte au même endroit vu que personne n’aura franchi la porte.  

![Photo calle porte détection](images/calle_porte_detection.png)

##### Détecteur de lumière

Le deuxième prototype repose sur un détecteur de lumière installé dans le logement, ce détecteur s’active dès qu’une source lumineuse est détectée dans le logement, indiquant ainsi si le bien est habité ou non.  

![Photo détecteur lumière](images/detecteur_de_lumiere.png)

Malheureusement ce dispositif soulève des questions potentielles sur la vie privée et la légalité, le fait de surveiller l’activité lumineuse à l’intérieur d’un espace privé peut être considéré comme une intrusion dans la sphère personnelle des occupants. 

##### Détecteur d'alarme 

Le troisième est un détecteur d’alarme de sécurité. Son concept réside en un dispositif pour vérifier la présence d’une alarme de sécurité dans le logement, permettant d’évaluer la facilité d’entrer.  

![Photo du détecteur d’alarme](images/detecteur_alarme.png)

Cette idée soulève également des préoccupations potentielles en termes de légalité. L’accès et l’analyse des dispositifs de sécurité d’un logement peuvent être considérés comme une intrusion à la vie privée des occupants, les lois sur la confidentialité et la sécurité pourraient être violées par une telle méthode. 


#### Occupation des logements 


Une « calle » de sécurité visant à renforcer la sécurité des occupants d’un logement, particulièrement cruciale dans des contextes où la vulnérabilité est élevée. L’idée est de permettre aux personnes qui squattent de verrouiller leur espace depuis l’intérieur, offrant ainsi une couche supplémentaire de sécurité. 

![Photo de la calle de sécurité ](images/calle_porte_occupation.png)

##### Sac de couchage chauffant sécurisé

Enfin, le sac de couchage chauffant avec une poche intégrée se présente comme une réponse globale aux défis auxquels sont confrontés non seulement les personnes vivant à la rue mais aussi celles des occupant des logements sociaux. Ce sac de couchage offre une combinaison unique de fonctionnalité pour améliorer le confort, la sécurité, et la praticité. 

- Chauffage : répond aux besoins des personnes vivant dans des conditions précaires que ce soit en squat ou à la rue, elle vise à offrir une source de chaleur essentielle pendant les périodes de froid 
- Poche intégrée : répond à plusieurs besoins, que ce soit pour les personnes sans-abris où les personnes vivant dans des logements sociaux (où il y a beaucoup de passage), la poche sécurisée offre une solution pratique pour protéger les effets personnels des résidents. 
 
![Photo du sac de couchage	](images/sac_de_couchage.png)
 
### Conclusion Hackaton

Cette démarche collaborative, guidée par la pression temporelle d’un hackathon, a permis à l’équipe de repousser les limites de la réflexion et de la créativité présentant des prototypes novateurs  

## Cheminement des pensées


### Une solution précise pour une problématique large

Après la semaine prototype en carton et les quelques réunions, nous avons conclu que notre projet se trouverait dans l'un des quatre axes des la liste ci-dessous. Certains axes peuvent être considérés comme des sous axes.

- Stabilité : Détection de problème de sécurité d'infrastructure.
- Insalubrité : Détection et/ou solution aux problèmes apportée par un lieu portant sur la santé des occupants.
  - Parasitisme : Détecter ou remédier aux problèmes de parasitismes.
- Sécurité : Sécurisation personnelle ou matérielle.

Nous ne pouvions pas avancer sans nous limiter à un seul axe. Nous avons donc procédé à un vote parmi ceux listé.

### Choix de l'axe et précision de l'idée de solution

Lors d'une réunion matinale, nous avons choisi sans trop de mal l'axe de l'insalubrité. Après quelques recherches, c'est l'axe qui nous parait le mieux adapté étant donné que nous devons sortir une solution matérielle en réponse à notre problématique.

Nous avons pensé à produire un détecteur d'insalubrité qui mesurerait par exemple la température, l'humidité et les particules dans l'air d'un appartement. 

Lors d'une entrevue avec Denis, il nous a fait remarquer qu'un détecteur ne servait pas à grand chose si après il n'existait pas de charte d'insalubrité où l'on pouvait comparer les valeurs enregistrées aux valeurs critiques pour ensuite étiquetter un logement comme insalubre. L'objectif ici est d'afficher les bâtiments vides insalubres pour pousser les propriétaires à entreprendre des travaux et remettre leurs bâtiments à la vente. 


### Recherche de mentor

Etant donné qu'il est difficile de lier notre problématique à une solution technique, il est imporant de trouver un.e mentor qui nous permettra de consolider notre vision de la problématique dans tous ses enjeux et ce qu’elle implique.

- [Etienne Toffin](etienne.toffin@ulb.be)  
- [Charlotte bens](https://communa.be/equipe/charlotte-bens/)  
- [Nola](https://communa.be/les-lieux/sorocite/) (ref par charlotte)  
- [Sarah De Laet](https://conferences-gesticulees.net/conferences/jhabite-tu-habites-ils-speculent/)  

Etienne a contacté pour nous Charlotte qui nous a malheureusement donné une réponse négative (temps et connaissance de personnes plus pertinentes selon elle). Eliott a alors contacté Nola et Sarah de Laet. Nola a répondu positivement pour une rencontre.

### Rencontre avec notre mentor Nola 

Nola a fait des études de gestion, avec un master complémentaire en gestion de l'environnement à l'ULB. Elle travaille maintenant comme coordinatrice de projets à Communa, une ASBL qui s’engage pour une ville plus abordable, plus démocratique, plus résiliente et plus créative, en gérant la logistique et la mise en place d'occupations temporaires. 

Nous l'avons rencontrée à Maxima, un grand lieu à Forest qui est en occupation temporaire depuis 4 ans. Il est composé d’un hébergement collectif pour femmes, de bureaux partagés, d’ateliers d’artistes et artisans.

Elle travaille aussi sur le projet Sorocité qui consiste en la rénovation de vieux appartements des années 70 vides à Evere offrant un logement et un espace collectif à des femmes qui se trouvaient dans une situation de sans-abrisme pendant 7 ans. 

Grâce à son expérience dans l'occupation temporaire de logements vides, Nola nous éclairé sur différents aspects et enjeux de l'occupation :

- Squat et occupation : le squat a une connotation péjorative car le terme désigne plus souvent les occupations sans accords avec les propriétaires, alors que c'est la base légale pour l'occupation de logements innocupés.

- Les propriétaires demandent, parfois, pour qu'une association d'occupation temporaire comme Comuna prenne en charge un bâtiment. Les avantages pour les proprios:  


  * Maitrise d'usage: Demande à ce qu'un certain type d'occupation soit tenu pour pouvoir juger de la possibilité de succès d'un type d'installation, à terme.
  * Maintien du bâtiment = Spéculation sereine
  * Évitement du squat sauvage

- Si les propriétaires veulent faire partir les personnes qui squattent leur logement, les propriétaires déclarent le bâtiment comme insalubre et la police expulse tout le monde !

Au final, détecter l'insalubrité des logements inoccupés pourraient aller à l'encontre de l'occupation des logements vides. Cette nouvelle a mis l'ensemble du groupe d'accord pour s'éloigner de l'axe de l'insalubrité. 

Après cette exploration large et profonde de la problématique du manque d'accessibilité aux logements à Bruxelles, nous nous sommes dirigés vers notre projet final avec l'idée de produire un outil de sensibilisation. 

## Projet final

En choisissant ce sujet, nous avons rapidement constaté la pluralité des causes de la crise du logement, leur interconnexion, et la complexité croissante de l'«arbre» a problèmes qui  s’est très vite transformé en une multitude de cycles (une personne avec moins de moyens aura plus de chance de se trouver dans un logement précaire donc précarisant donc aura encore plus de chance de se trouver dans quelque chose de plus insalubre/ mal entretenus ou sur longue liste d’attente de logements sociaux etc.).     

Malgré nos efforts pour aider directement les habitants/ sans abris/ occupants avec une «solution pansement» (parce que la vraie solution à la crise du logement à Bruxelles est une solution politique, qui s’occuperait de reloger de manière permanente les personnes sans-abris, un contrôle sur l’augmentation des prix des loyers, une réquisition des logements vide, un meilleur accompagnement aux aides sociale, une régularisation des personnes sans papier,...) avec un peu de recul, toutes les idées de solutions technique que nous avions seraient soit à la limite de la légalité (aider à l’occupation des bâtiments), futiles (un cadenas/ alarme pour les affaires des personnes sans-abris), inefficaces voire complètement contre-productives pour les habitants (détecteurs d’insalubrités, certains propriétaires utilise l’insalubrité de leur biens pour expulser leur locataires, occupants). 

Nous nous sommes mis d’accord que pour un changement politique il faut commencer par une sensibilisation « par dispositifs de sensibilisation, il faut entendre, plus précisément, l’ensemble des supports [..] que les militants déploient afin de susciter des réactions affectives qui prédisposent ceux qui les éprouvent à s’engager ou à soutenir la cause défendue » [Traïni, Christophe Siméant-Germanos, Johanna  Introduction. Pourquoi et comment sensibiliser à la cause ? DOI: 10.3917/scpo.train.2009.01.0011], nous avons donc décidé de faire un «arbre a problèmes en quatre dimensions», les deux premières dimensions étant les dimensions de l’arbre tel que nous l’avions dessiné au début, la 3e représente les tiroirs, portes, fenêtres et volets dans lesquelles se trouves des témoignages, exemples, cycles, solutions. La quatrième dimension est celle des autres sens, tiroirs sonores, fenêtre lumineuse, etc... 

Le but de cette maquette serait d’en faire un outil éducatif pour toutes personnes s’intéressant de près ou de loin à la crise du logement à Bruxelles et qui voudrait avoir une vision globale du problème pour après comprendre chaque sous problématiques. Ces mêmes personnes seront celleux qui voteront (peut-être) pour des politiques de relogements, qui accepteront plus facilement qu’un logement social soit construit dans leurs quartiers  

Art. 25 de la déclaration universelle des droits de l’homme : 1. Toute personne a droit à un niveau de vie suffisant pour assurer sa santé, son bien-être et ceux de sa famille, notamment pour l'alimentation, l'habillement, le logement, les soins médicaux ainsi que pour les services sociaux nécessaires ; elle a droit à la sécurité en cas de chômage, de maladie, d'invalidité, de veuvage, de vieillesse ou dans les autres cas de perte de ses moyens de subsistance par suite de circonstances indépendantes de sa volonté.

### Pré-jury

Nous devons pour ce pré-jury fournir une preuve de concept. Nous avons comme objectif de donner à notre outil de sensibilisation final une apparence de bâtiment. Pour le pré-jury, nous avons produit une version simplifiée qui s'apparente à une structure de 2 étages contenant 4 tiroirs :  

* Les inégalités sociales et économiques
* Les logements sociaux 
* La gentrification
* L'insalubrité  
  

---

![im2](images/Tiroir_Pré-jury.png)
![](images/Tiroir_Pré-jury.png)

Ci-dessus, le tiroir sur la gentrification à Bruxelles.    

![im1](images/Maison_Pré-jury.png)
![](images/Maison_Pré-jury.png)

Voici l'objet que nous avons présenté au pré-jury. Sur le dessus nous pouvons apercevoir la représentation de St-Vide, la 20ème commune de Bruxelles fictives. Celle-ci contient l'équivalent de tous les logements inoccupés à Bruxelles et a une superficie plus grande que Ixelles. Nous avons ajouté une puce nfc qui renvoit directement vers le slide présentant notre projet. 

#### Retour

Les retours ont été assez positifs ! Cependant un gros point à améliorer est l'auto-suffisance de notre objet. En effet, lors de la présentation le pré-jury n'avait pas une idée claire de notre objet. Il a fallu que nous soyons là pour le présenter alors que l'objectif est de laisser l'objet dans un lieu public que les passant.es s'y intérressent. 

## Préparation au Jury final 

Après donc avoir eu tous ces retours, nous avons approfondis les différentes idées visuellles que nous pourions mettre dans notre maison finale. Nous avions, comme idées visuelles :

- des pop-up
- une maison de la forme d'une maison
- un ticket de caisse représentant le manque d'argent
- des collonnes de fluide représentant différéntes valeurs (comme la construction de logements de luxe vs construction de logements sociaux)
- des pièces avec de fenêtre en plexiglass
- des pièces "poupées russes"
- lumières
- son

dans le but d'avoir une maison la plus pédagogique possible.

Nous nous sommes répartis les différents moyens sensoriels en fonction des différéntes problématiques, et nous les avons répartis dans la maison.

Nous avons donc :

### un tiroir insalubrité

Le tiroir insalubrité se compose d'un livret accordéon expliquant les differents tyês d'insalibrité, les loi en vigeur à ce propos, et que faire en cas d'insalibrité et d'un circuit éléctronique émettant du son (bruits de villes, travaux, voisinage) dès que le livret est ouvert à la page "isolation accoustique et thermique".

![](images/insal.png)

### un tiroir inégalité

Dans le tiroir inégalité, il est expliqué les raisons des inégalités sociales et économiques et les discriminations à l'accès aux logement. Les inégalités économiques sont représentées par un ticket de caisse.

![](images/gala.png)

### un tiroir logements sociaux


Le tiroir logements sociaux met en évidence l'écart entre la demande de logement sociaux et l'offre mise en place par la ville. De plus il est expliqué que le ma grande majoritée des nouvelles constructions apartiennent à des organismes privés.

![](images/eliott.png)

### un grenier gentrification

Le grenier s'occupe de représenter la gentrification, les principaux acteurs de la gentrification sont des organismes privés faisant des nouvelles constructions et de la spéculation économique. Quoi de mieux pour représenter ça qu'un jeu de monopoly. Le grenier se compose donc d'un jeu de Monopoly représantant différents quartiers gentrifiés à Bruxelles.


![](images/monop.png)


### un hangar vide et insalubre

Ça va de soi, les logements sont vides, tout comme notre hangar.

### Façade Bruxeles

Nous avons surlignés la commune de Ixelles dans une carte de Bruxelles pour sousligner la superficie des logements vides inspirées par [la Vingtième commune de Bruxelles](https://www.leegbeek.brussels/).

### La maison 


La maison a été complètement dessinée par nos soins sur Fusion 360, après s'être mis d'accord sur les dimensions voulues des différents tiroirs. Puis à été découpée en mdf à la découpeuse laser. 

Nous avons eu quelques problèmes de dimensionnements en découpant au laser (les fichiers passant par Illustator n'étaient pas à la même proportion que les fichiers passées par inkscape), de plus, le fichier de 3D se perdait dans les méandres des serveurs AutoCad, plusieurs fois nos avancées se sont donc perdues. 

Après toutes ces péripéties informatique, nos avons finalement fini par découpée notre maison, l'assembler et y ajouter nos informations éducatives dans les différents tiroirs.

Celle-ci est accompagnée d'un circuit électronique lumineux avec plusieurs led avec "effet faux contact" son mises dans le hangar et dans le toit.

Et voici notre maison en entière et en vidéo que nous pouvons tous admirer ensemble : 

<center>
<video width="360" height="640" controls>
  <source src="../images/videomaison.mp4" type="video/mp4">
  <source src="movie.ogg" type="video/ogg">
Your browser does not support the video tag.
</center>