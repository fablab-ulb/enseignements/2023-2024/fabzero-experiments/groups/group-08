# Groupe et présentation du projet

## Notre groupe
Bienvenue sur la page de notre projet de groupe.
Notre équipe se compose de :

- [Milan Bontridder](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/milan.bontridder) BA3 Informatique
- [Eva Dubar](https://eva-dubar-fablab-ulb-enseignements-2023-2024-fab-47725489fd9b1e.gitlab.io/) MA2 Ingénieure informatique
- [Gala Toscano-Aguilar](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/gala.toscano) BA3 Bioingénieure
- [Eliott Fontaine](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/eliott.fontaine) BA3 Bioingénieur

Nous nous sommes choisi.es le jour de la création des groupes, et réuni.es autour du sujet du manque de logements à Bruxelles. 

![](images/photo-group.png)

Voici une superbe photo de nous, juste avant le jury (on est pas du tout stressé.es)

Nous sommes des étudiants de l'ULB venant de filières différentes. Cependant, nous avons un cours en commun: [How To Make (almost) Any Experiment Using Digital Fabrication](https://class-website-fablab-ulb-enseignements-2023-2024-c4a40531a3337f.gitlab.io/). 




## Résumé de notre projet

Notre projet se focalise sur la sensibilisation au manque de logement dans Bruxelles.

![Graphical abstract](images/group-08.png)

Nous avons choisis le sujet "Pourquoi tout le monde n'a pas accès à un logement" le jour des créations des groupes et sommes passés par un nombre incalculables de péripéties (toutes expliquée dans la partie proccesus) avant de trouver notre projet final, lié à la sensibilisation au sujet.

Après s'être essayé.es à plusieurs outils technique solutionnant une partie du problème de la crise des logements à Bruxelles, et s'être rendu.es compte qu'aucune de ces solutions ne solutionnerai le problème (regretable pour une solution). Nous nous sommes rendu.es comptes que le problème etait surtout un problème politique, et nous nos sommes donc redirigé.es vers une solution éducative, de sensibilisation aux différentes causes du manque de logements, en vue d'un changement et d'une prise de conscience collective.


Notre objet/ projet final est donc une maison éducative, expliquant les causes du manque de logements à Bruxelles. Pour créer notre maison nous avons 
- lu des études sociologiques
- rassemblé des sous problématiques dans le but d'une explication pédagogique
- designé des illustrations de problématiques
- dessiné une maison découpée au laser
- crée un circuit électronique
- imprimé des objets en 3d





Nous nous sommes aussi confronté.es à des problèmes techniques (ordinateur qui perd les fichers 3D sans sauvegarder - a répétition, problème d'échelle dans les découpe, ...)

Mais, après être tombé.es, relevé.es, retombé.es, persévéré.es, nous sommes parvenu.es à créer une maison de toute pièce comportants divers témoignages et explications de problématiques, représentant de manière plus ou moins visuels différentes causes, et conçue avec différents outils du FabLab.

Nous pouvons à présent admirer notre magnifique maison &#129321; : 

![](images/maison_finale.png)

<center>
<video width="360" height="640" controls>
  <source src="../images/videomaison.mp4" type="video/mp4">
  <source src="movie.ogg" type="video/ogg">
Your browser does not support the video tag.
</center>
