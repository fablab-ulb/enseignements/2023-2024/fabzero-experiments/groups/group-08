# COntraintes, problèmes et ressources
Pour la semaine prototype en cartons, nous avons organisé un stand avec des prototypes représentatifs et non fonctionnels de notre problèmatique.
Les prototypes étaient disposé sur la table avec les fiches suivantes. 

## Contraintes

- Réponse partielle ou complète à la problématique (la problématique étant les problèmes de manque d'habitations).
- Accessibilité (budget)
- Localisé à Bruxelles
- 


## Problèmes

- arbre à problème (encore très ouvert pour l'instant)

***todo. Il n'y avait encore rien sur la feuille***

## Ressources
***todo. Il n'y avait encore rien sur la feuille***



### Ressources potentielles

- [FéBUL](https://duckduckgo.com/?q=febulle+bruxelles&t=newext&atb=v391-1&ia=web)
- [Cocreate brussels](https://www.cocreate.brussels/)
- Charlotte de la Serre mise en contacte par Etienne Toffin (etienne.toffin@ulb.be). -> a refusé
  - lien vers des recommendations

### Ressources utilisées
