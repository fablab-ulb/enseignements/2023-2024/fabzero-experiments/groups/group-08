# 2023-12-11 PV réunion avec Denis de 30 minutes.

## Contexte

- Réunion avec Denis le mardi de la semaine précédent le pré-jury.
- Tout le monde présent.
- Gala 5 minutes en retard.


## sujet

### Insalubrité

- gaz
	- CO
	- CO2
	- éthanol
	- ammoniac
	- alcool
	- benzène
	- oxyde d'azote
- particules
	- amiante ?
	- PM10
	- PM25
- humidité
- température
* vibration
* parasite
	* rats...
	* puces de lits

## utilité

- Avoir une charte des choses qui déterminent l'insalubrité d'un lieu.
- L'idée du capteur de base était trop vague (comment interpréter les données recueillies ?)
- -> Utilité finale du capteur ? Ce sur quoi il agit concrètement. Est-ce applicable ?

## demandes

Important :
prototype demandé : **preuve de concepte**:
quelque chose fonctionne. Voir si l'idée fonctionne ou non.
