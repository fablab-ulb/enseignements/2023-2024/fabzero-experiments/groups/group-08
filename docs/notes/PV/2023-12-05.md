# 2023-12-5 PV

Le but de cette réunion est de choisir absolument un axe.

PV : Eliott.

## Rôles

* Secrétaire : Eliott
* T check : Milan 
* Animation : Eva
* Timing : Gala

## T check

* Eliott fatigué mais présent
* Eva a taffé jusque 4h30 samedi mais a rendu la plupart de ses projets!! Elle vient de passer 50min dans le 81. Positif : son réveil ne fut pas aussi terrible qu'elle eut pensé
* Gala a pris un café et ça a coupé sa faim. Elle va très bien et a rendu un travail dimanche. Malheureusement, elle n'a pas passé une bonne soirée samedi
* Milan brûle

## Exploration des idées

Nous devions pour cette réunion explorer les différentes idées au sein des axes qu'on prendrait potentiellement pour préciser notre réponse à la problématique des logements inoccupés à Bruxelles.

### Occupation

* Détecteur d'alarmes : Très chaud à faire et illégal :

* Cale : Trop simple à faire. Ali a dit un truc smart que faisait ses potes: Mettre des pubs

* Sac de couchage fermé : il en existe avec batterie. Ce serait coll d'en faire sur batterie. Il y a l'idée de la chaufferette mais on n'a pas de labo à dispo et c'est toxique

### Insalubrité

* Accéléromètre, capteur de mouvement pour l’instabilité. Ce serait plutôt un gyroscope.
Gala : on pourrait faire un détecteur qui intègre humidité et mouvement (+ capteur parasite).
Il y a un tuto pour construire un moniteur de qualité de l’air : particules, humidité et gyromètre.
Gala l’envoie sur le groupe et ça a l’air faisable.


* Idée de Milan : sac de couchage ultra-valable pour se débarrasser plus facilement des parasites.
C’est compliqué si pas d’accès à une machine à laver. Ça pourrait être un tissu qui repousse les parasites aussi.
À creuser ?

---
" Un ours, tu le regardes t'es dans les pommes. " *Milan 2023*

---

**Décision faite au consensus:** On prend l'axe de l'insalubrité !

## Précision de l'idée de solution

-> Go sur le capteur humidité, particules, et on ajoute le gyroscope.
Grosse partie programmation. Il faudrait que l’info soit accessible. 
Ce ne serait pas pour les gens qui squattent. L’objectif c’est que recenser l’insalubrité soit + accessible.
Milan est confortable avec ça, il a déjà fait des projets similaires.
On peut faire une représentation schématique électronique et ça donne une autre fenêtre avec des liens entre les composants.
EasyEda permet aux utilisateurs de créer des schémas électroniques.

## To do 

* Milan et Eva : Recherche
* Gala et Eliott : Documentation
* Eliott suite recherche mentor
* Photo de groupe 😎
